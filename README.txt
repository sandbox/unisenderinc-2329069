ABOUT
================================================================================
UniSender - a popular system of mass email and SMS mailing list that allows you to:
1. Perform reliably send email and SMS messages;
create professional emails and SMS messages, even if you do not get along with html code, using the convenient block editor;
2. sort out your base to the segments and to make special unique offers;
make beautiful and effective forms of subscriptions that you can put on your website, blog or page in the social. network;
3. configure automatic series of letters that can sell almost without your intervention;
to experiment, learn what letter your customers better pay attention, to conduct A / B testing, and to increase their efficiency;
4. combine email and SMS mailing;
5. observe the distribution in real time;
6. receive detailed reports after the mailing of who and how many times opened the letter, passed by reference, made ​​a booking and much more;
7. work in a team, giving access to your account with a different level of rights for their colleagues;
8. order a customized email-strategy and / or download free ready-made solutions for your business;
9. with large volumes of distribution we will give you a personal account manager who will help you at any time and will answer all questions about the service;
10. at any convenient time for you, you can take the free course on email-marketing. Knowledge will help you to improve efficiency and implement new chips in your mailing list;
11. email-to commission an audit strategy or consult with email-marketer;
12. and much more.

SUPPORT
================================================================================
Write to us: plugins@unisender.com